(function () {
    'use strict';

    angular.module('app').controller('decaCtrl', Ctrl);


    function Ctrl($http, unitService) {

        var url = 'http://localhost:8080/api/';
        var vm = this;
        vm.selectedEvent = null;
        vm.events = [];
        vm.resultVal = 0;
        vm.pointsValue = 0;
        vm.unit = '';

        vm.addPoints = addPoints;
        vm.initUnit = initUnit;

        initDropDown();

        function initDropDown() {
            $http.get(url + 'events').then(function (response) {
                vm.events = response.data;
                vm.selectedEvent = vm.events[0].value;
            });
        }

        function addPoints() {

            var newPoints = {};

            if ((vm.selectedEvent) !== undefined) {
                newPoints = {
                    eventId: JSON.parse(vm.selectedEvent).id,
                    resultValue: vm.resultVal
                }
            } else {
                newPoints = {
                    eventId: undefined,
                    resultValue: vm.resultVal
                }
            }

            $http.post(url + 'points', newPoints).then(function (response) {
                vm.pointsValue = response.data.points;
            });
        }

        function initUnit() {
            vm.unit = unitService.getUnit(JSON.parse(vm.selectedEvent).unit);
        }

    }
})();