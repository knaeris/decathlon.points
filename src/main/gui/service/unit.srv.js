(function () {
    'use strict';

    var units = {
        'm': 'meetrites',
        's': 'sekundites'
    };

    angular.module('app').service('unitService', Srv);

    function Srv() {

        this.getUnit = getUnit;

        function getUnit(unitKey) {

            var longUnit = units[unitKey];

            return longUnit;
        }
    }

})();