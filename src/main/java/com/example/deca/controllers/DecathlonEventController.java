package com.example.deca.controllers;

import com.example.deca.dao.interfaces.IDecathlonEventDao;
import com.example.deca.domain.DecathlonEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DecathlonEventController {

    @Autowired
    private IDecathlonEventDao dao;

    @CrossOrigin
    @GetMapping("api/events")
    public Iterable<DecathlonEvent> getAllDecathlonEvents(){
        return dao.findAllDecathlonEvents();
    }

    @GetMapping("api/events/{id}")
    public DecathlonEvent getDecathlonEventById(@PathVariable("id") Long id){
        return dao.findDecathlonEventById(id);
    }


}
