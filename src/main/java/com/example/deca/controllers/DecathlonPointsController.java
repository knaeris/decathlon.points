package com.example.deca.controllers;

import com.example.deca.dao.interfaces.IDecathlonPointsDao;
import com.example.deca.domain.DecathlonPoints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DecathlonPointsController {

    @Autowired
    private IDecathlonPointsDao dao;

    @GetMapping("api/points/{id}")
    public DecathlonPoints getDecathlonPointsById(@PathVariable("id") Long id){
        return dao.findDecathlonPointsById(id);
    }

    @CrossOrigin
    @PostMapping("api/points")
    public DecathlonPoints savePoints(@RequestBody DecathlonPoints decathlonPoints){
        dao.savePoints(decathlonPoints);
        return getDecathlonPointsById(decathlonPoints.getId());
    }


}
