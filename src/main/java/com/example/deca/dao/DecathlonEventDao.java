package com.example.deca.dao;

import com.example.deca.dao.interfaces.IDecathlonEventDao;
import com.example.deca.domain.DecathlonEvent;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class DecathlonEventDao implements IDecathlonEventDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<DecathlonEvent> findAllDecathlonEvents() {
        return em.createQuery("select  e from DecathlonEvent e",
                DecathlonEvent.class).getResultList();
    }

    @Override
    public DecathlonEvent findDecathlonEventById(Long id) {

        TypedQuery<DecathlonEvent> query = em.createQuery("select e from DecathlonEvent e where e.id= :id", DecathlonEvent.class);
        query.setParameter("id",id);
        return query.getSingleResult();
    }



}
