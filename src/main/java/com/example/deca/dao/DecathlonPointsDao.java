package com.example.deca.dao;

import com.example.deca.dao.interfaces.IDecathlonEventDao;
import com.example.deca.dao.interfaces.IDecathlonPointsDao;
import com.example.deca.domain.DecathlonEvent;
import com.example.deca.domain.DecathlonPoints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

@Repository
public class DecathlonPointsDao implements IDecathlonPointsDao {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private IDecathlonEventDao dao;

    @Transactional
    public void savePoints(DecathlonPoints decathlonPoints) {
        decathlonPoints.setEvent(dao.findDecathlonEventById(decathlonPoints.getEventId()));
        DecathlonEvent event = decathlonPoints.getEvent();
        float resultValue = decathlonPoints.getResultValue();
        float parameterA = event.getParameterA();
        float parameterB = event.getParameterB();
        float parameterC = event.getParameterC();
        double points = 0.0;

        if(!event.isFieldEvent()){
            switch(event.getEventName()){
                case "Kaugushüpe":
                case "Kõrgushüpe":
                case "Teivashüpe":
                    points = parameterA*Math.pow((resultValue*100-parameterB), parameterC);
                    break;
                default:
                    points = parameterA*Math.pow((resultValue-parameterB), parameterC);
            }
            decathlonPoints.setPoints((long) points);

        } else {
            points = parameterA*Math.pow((parameterB-resultValue), parameterC);
            decathlonPoints.setPoints((long) points);
        }

        em.persist(decathlonPoints);
    }

    @Override
    public DecathlonPoints findDecathlonPointsById(Long id) {
        TypedQuery<DecathlonPoints> query = em.createQuery("select p from DecathlonPoints p where p.id = :id", DecathlonPoints.class);
        query.setParameter("id",id);
        return query.getSingleResult();
    }
}
