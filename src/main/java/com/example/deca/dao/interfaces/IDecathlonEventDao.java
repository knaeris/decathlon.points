package com.example.deca.dao.interfaces;

import com.example.deca.domain.DecathlonEvent;
import com.example.deca.domain.DecathlonPoints;

import java.util.List;

public interface IDecathlonEventDao {

List<DecathlonEvent>  findAllDecathlonEvents();

DecathlonEvent findDecathlonEventById(Long id);


}
