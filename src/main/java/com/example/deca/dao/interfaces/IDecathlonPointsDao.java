package com.example.deca.dao.interfaces;

import com.example.deca.domain.DecathlonPoints;

public interface IDecathlonPointsDao {
    void savePoints(DecathlonPoints points);

    DecathlonPoints findDecathlonPointsById(Long id);
}
