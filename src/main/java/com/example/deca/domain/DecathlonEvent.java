package com.example.deca.domain;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name="decathlon_event")
public class DecathlonEvent {
    @Id
    @SequenceGenerator(name = "seq", sequenceName = "event_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    private Long id;

    @Column(name="event_name")
    private String eventName;

    @Column(name="field_event")
    private boolean fieldEvent;

    @Column(name="unit")
    private char unit;

    @Column(name="parameter_a")
    private float parameterA;

    @Column(name="parameter_b")
    private float parameterB;

    @Column(name="parameter_c")
    private float parameterC;
}
