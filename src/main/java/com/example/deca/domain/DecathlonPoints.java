package com.example.deca.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name="decathlon_event_points")
public class DecathlonPoints {
    @Id
    @SequenceGenerator(name = "seq", sequenceName = "points_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    private Long id;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="event_id", insertable = false, updatable = false)
    @JsonIgnore
    private DecathlonEvent event;

    @NotNull
    @Column(name="event_id" )
    private Long eventId;

    @Column(name="result_value")
    private float resultValue;

    @Column(name="points")
    private Long points;

    public DecathlonPoints(@NotNull Long eventId, float resultValue) {
        this.eventId = eventId;
        this.resultValue = resultValue;
    }
}
