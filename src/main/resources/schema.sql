 DROP SCHEMA PUBLIC CASCADE;

 CREATE SEQUENCE EVENT_SEQ START WITH 1;
 CREATE SEQUENCE POINTS_SEQ START WITH 1;

   CREATE TABLE DECATHLON_EVENT(
     ID BIGINT PRIMARY KEY NOT NULL,
     EVENT_NAME VARCHAR(255) NOT NULL,
     FIELD_EVENT BOOLEAN NOT NULL,
     UNIT CHAR NOT NULL,
     PARAMETER_A FLOAT NOT NULL,
     PARAMETER_B FLOAT NOT NULL,
     PARAMETER_C FLOAT NOT NULL
   );

    CREATE TABLE DECATHLON_EVENT_POINTS(
    ID BIGINT PRIMARY KEY NOT NULL,
    EVENT_ID BIGINT NOT NULL,
    RESULT_VALUE FLOAT NOT NULL,
    POINTS BIGINT NOT NULL,
    FOREIGN KEY (EVENT_ID)
        REFERENCES DECATHLON_EVENT(ID) ON DELETE CASCADE
    );

