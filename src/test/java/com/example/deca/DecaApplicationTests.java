package com.example.deca;

import com.example.deca.domain.DecathlonPoints;
import com.example.deca.model.Result;
import com.example.deca.util.PenaltyOnTestFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DecaApplicationTests extends Abstract{

    private final String BASE_URL = "http://localhost:8080/";

    @Test
    @PenaltyOnTestFailure(10)
    public void baseUrlResponds() {
        boolean isSuccess = sendRequest(getBaseUrl());

        assertThat(isSuccess, is(true));
    }

    @Test
    @PenaltyOnTestFailure(10)
    public void calculatesPoints() {

        DecathlonPoints points = new DecathlonPoints(1L,9.9f);

        Result<DecathlonPoints> result = postPoints("api/points", points);
        Long idOfPostedPoints = result.getValue().getId();

        DecathlonPoints read = getOne("api/points/"+ idOfPostedPoints);

        assertThat(read.getPoints(), is(1121L));
    }

    @Override
    protected String getBaseUrl() {
        return BASE_URL;
    }
}

